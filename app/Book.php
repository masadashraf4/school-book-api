<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model{

    protected $fillable = [
        'section',
        'school',
        'class',
        'is_full_course',
        'quantity',
        'is_donate',
        'is_sell',
        'amount',
        'latitude',
        'longitude',
        'contact',
        'user_id'
    ];

    protected $hidden = ['deleted_at'];

    protected $casts = [
        'amount' => 'double',
        'latitude' => 'double',
        'longitude' => 'double',
        'is_full_course' => 'boolean',
        'is_donate' => 'boolean',
        'is_sell' => 'boolean'
    ];

    protected $with = ['books'];

    public function books(){
        return $this->hasMany(BookImage::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }


    public function getDistanceAttribute($value){
        return round($value,2);
    }

    public function scopeIsWithinMaxDistance($query, $coordinates, $radius = 5) {

        $haversine = "(6371 * acos(cos(radians(" . $coordinates['latitude'] . ")) 
                    * cos(radians(`latitude`)) 
                    * cos(radians(`longitude`) 
                    - radians(" . $coordinates['longitude'] . ")) 
                    + sin(radians(" . $coordinates['latitude'] . ")) 
                    * sin(radians(`latitude`))))";

        return $query->select('*')
            ->selectRaw("{$haversine} AS distance");
    }
}