<?php

namespace App\Http\Controllers;

use App\Book;
use App\BookImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{


    private $book;

    public function __construct(Book $book)
    {
        $this->book = $book;
    }


    public function createPost(Request $request)
    {
        $bookData = array();
        $validator = Validator::make($request->all(), [
            'section' => 'required|string|max:255',
            'school' => 'required|string|max:255',
            'class' => 'required|string|max:255',
            'contact' => 'required|string|max:15',
            'is_full_course' => 'required|in:true,false',
            'quantity' => 'required_if:is_full_course,false|int',
            'is_donate' => 'required_if:is_sell,false|in:true,false',
            'is_sell' => 'required_if:is_donate,false|in:true,false',
            'amount' => 'required_if:is_sell,true|numeric',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return $this->response($validator->errors(), 'Invalid data given', 422);
        }

        try {
            if (!$request->has('is_sell')) {
                $request->request->add(['is_sell' => false]);
            }
            $params = $request->all();

            $iteration = 6;
            for ($i = 1; $i <= $iteration; $i++) {

                if ($request->has('book_image_' . $i)) {
                    $name = "";
                    $book = $params['book_image_' . $i];
                    $imageName = $this->generateRandomString(20) . time() . '.' . $book->getClientOriginalExtension();
                    $book->move(base_path('public/photos'), $imageName);
                    if ($request->has('book_name_' . $i)) {
                        $name = $params['book_name_' . $i];
                    }
                    $bookData[] = new BookImage(array('image' => 'photos/' . $imageName, 'name' => $name));
                }

            }


            foreach ($params as $value => $key) {
                if ($key == "true" || $key == "false") {
                    $params[$value] = filter_var($key, FILTER_VALIDATE_BOOLEAN);
                }
            }

            $book = $this->book->create($params);

            if (count($bookData) > 0) {
                $book->books()->saveMany($bookData);
            }
            $book = $this->book->find($book->id);
            return $this->response($book);
        } catch (\Exception $e) {
            return $this->response([], $e->getMessage(), 500);
        }

    }


    public function create(Request $request)
    {
        $images = array();
        $validator = Validator::make($request->all(), [
            'section' => 'required|string|max:255',
            'school' => 'required|string|max:255',
            'class' => 'required|string|max:255',
            'contact' => 'required|string|max:15',
            'is_full_course' => 'required|in:true,false',
            'quantity' => 'required_if:is_full_course,false|int',
            'is_donate' => 'required_if:is_sell,false|in:true,false',
            'is_sell' => 'required_if:is_donate,false|in:true,false',
            'amount' => 'required_if:is_sell,true|numeric',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return $this->response($validator->errors(), 'Invalid data given', 422);
        }

        try {
            if (!$request->has('is_sell')) {
                $request->request->add(['is_sell' => false]);
            }
            $params = $request->all();
            if ($request->has('book_images')) {

                foreach ($request->book_images as $book) {
                    $imageName = $this->generateRandomString(20) . time() . '.' . $book->getClientOriginalExtension();
                    $book->move(base_path('public/photos'), $imageName);
                    $name = "";
                    $images[] = new BookImage(array('image' => 'photos/' . $imageName, 'name' => $name));
                }
            }

            foreach ($params as $value => $key) {
                if ($key == "true" || $key == "false") {
                    $params[$value] = filter_var($key, FILTER_VALIDATE_BOOLEAN);
                }
            }

            $book = $this->book->create($params);

            if (count($images) > 0) {
                $book->books()->saveMany($images);
            }
            $book = $this->book->find($book->id);
            return $this->response($book);
        } catch (\Exception $e) {
            return $this->response([], $e->getMessage(), 500);
        }

    }


    public function index(Request $request)
    {
        $coordinates['latitude'] = (double)$request->header('latitude') ?? 0.0000000;
        $coordinates['longitude'] = (double)$request->header('longitude') ?? 0.0000000;
        try {
            return $this->response($this->book
                ->isWithinMaxDistance($coordinates)
                ->orderBy('distance')->get());
        } catch (\Exception $e) {
            return $this->response([], $e->getMessage(), 500);
        }
    }

    public function getBookByType(Request $request, $type)
    {
        $res = [];
        $coordinates['latitude'] = (double)$request->header('latitude') ?? 0.0000000;
        $coordinates['longitude'] = (double)$request->header('longitude') ?? 0.0000000;
        Switch ($type) {
            case 'donate' :
                $res = $this->book->
                isWithinMaxDistance($coordinates)->
                where('is_donate', true)
                    ->orderBy('distance')
                    ->get();
                break;
            case 'sell' :
                $res = $res = $this->book
                    ->isWithinMaxDistance($coordinates)
                    ->where('is_sell', true)
                    ->orderBy('distance')
                    ->get();
                break;
            case 'full-course' :
                $res = $res = $this->book
                    ->isWithinMaxDistance($coordinates)
                    ->where('is_full_course', true)
                    ->orderBy('distance')
                    ->get();
                break;
            default:
                return $this->response([], 'Invalid type', 403);
        }

        return $this->response($res);
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}