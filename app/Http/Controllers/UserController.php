<?php

namespace App\Http\Controllers;

use App\Book;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function register(Request $request)
    {
        if (!isset($request->email)) {
            return $this->response([], 'Please provide email address', 422);
        } else if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            return $this->response([], 'Invalid email address', 422);
        } else if (!isset($request->password)) {
            return $this->response([], 'Please provide password', 422);
        }

        $user = $this->getUser($request->email);

        if ($user) {
            if ($this->userAuthentication($user->password, $request->password)) {
                $user->is_registered = false;
                return $this->response($user);
            } else {
                return $this->response([], 'Please enter correct password', 401);
            }
        } else {
            $user = $this->insert($request);
            if ($user) {
                $user->is_registered = true;
                return $this->response($user);
            } else {
                return $this->response([], 'Something went wrong', 500);
            }

        }
    }

    public function userAuthentication($userPassword, $enteredPassword)
    {
        if (Hash::check($enteredPassword, $userPassword)) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserBooks(Request $request){
        $id = $request->user_id ?? 0;

        $user = User::find((int)$id);
        if ($user){
            $books = Book::where('user_id', $user->id)->get();
                return $this->response($books);
        }else{
            return $this->response([],'No user found for this id',403);
        }
    }

    private function insert($request)
    {

        $user = new User;
        $user->name = isset($request->name) ? $request->name : '';
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return $user;
    }

    private function getUser($email)
    {

        return $this->model->where('email', $email)->first();
    }
}
