<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function response($data,$message = 'success',$code = 200){
       $response['code']  = $code;
       $response['message']  = $message;
       $response['data']  = !empty($data) ? $data : new \stdClass() ;


       return $response;
    }
}
