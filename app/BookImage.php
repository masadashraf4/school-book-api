<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookImage extends Model
{
    protected $fillable = ['book_id', 'image','name'];

    public function getNameAttribute($value){
        return $value ?? "";
    }
}