<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
#    return $router->app->version();

    return '<h1>School Booking App</h1>';
});


$router->post('/register', 'UserController@register');
$router->post('/login', 'UserController@register');

$router->post('/book', 'BookController@create');
$router->post('/post', 'BookController@createPost');
$router->get('/book', 'BookController@index');
$router->get('/book/{type}', 'BookController@getBookByType');
$router->get('/user/post', 'UserController@getUserBooks');

#$router->get('/uniform')